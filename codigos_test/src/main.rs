use std::io;

fn main() {
    let stdin = io::stdin(); // "acortar" el nombre de la funcion para no usar el io a cada rato

    // declarar sin dar valor
    //let mut num1 :i32;
    //let mut num2 :i32;

    let mut user_input = String::new();

    println!("Ingresa algo: ");
    stdin.read_line(&mut user_input).unwrap();

    println!("Ingresaste: {}", user_input);
    
    nada();
    let res :i32;

    res = suma(3, 4);

    println!("La suma es {}", res);

    let pedido :i32;
    
    pedido = pedir_numero();
    println!("Ingresaste el numero {}", pedido);

    let str_num = "101";
    let num: i32 = str_to_int(str_num.to_string());
    println!("el numero es {}", num);
    
    
}

// Definir otra funcion diferente de la main
fn nada(){
    println!("Hola chiquillos como están");
}

fn suma(x: i32, y: i32) -> i32 {
    x+y // importante no usar el ;
}


fn str_to_int(string: String) -> i32 {
    let trimmed = string.trim();
    let num: i32 = trimmed.parse().unwrap();
    num
}


// Pedir un numero entero y devolverlo
fn pedir_numero() -> i32{

    println!("Ingrese un numero:");
    let stdin = io::stdin();
    let mut user_input = String::new();
    stdin.read_line(&mut user_input).unwrap();
    let str_num = user_input.trim();
    let res: i32 = str_num.parse().unwrap();
    res
}
