mod ask_numbers {

    use std::io;
    let stdin = io::stdin();

    fn ask_int() -> i32{
	// se pide un numero entero

	let mut user_input = String::new();
	stdin.read_line(&mut user_input).expect("Expected to read line");
	let num:i32 = user_input.trim().parse().unwrap();
	num
    }

    fn ask_float()->f32{
	let mut user_input = String::new();
	stdin.read_line(&mut user_input).expect("Expected to read line");
	let num:f32 = user_input.trim().parse().unwrap();
	num
    }
    
}
