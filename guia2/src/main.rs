use std::io;
use rand::Rng;

fn main() {
    /*
    let mut rng = rand::thread_rng();

    let text = "este es el texto de prueba";
    ex01(text.trim().to_string());

    let palabra: String = "holabuenastardes".to_string();
    let letra: String = "a".to_string();
    let count: i32 = ex02(palabra.to_string(), letra.to_string());
    println!("La letra {} se encuentra {} veces en {}", letra, count, palabra);

    ex03("martinjuliangaetegarrido".to_string());
    ex04();
    let rnd: i32 = rng.gen_range(1..11);
    unam_ex05(rnd);
    
    let num = ex06(100, 100);
    println!("hola {}", num);
    
    let mut letter = ex07("holaamigos".to_string());
    println!("Letra es {}", letter);
    letter = ex08("holaamigos".to_string());
    println!("Letra es {}", letter);

    let clock = ex09(3, 49);
    println!("Las {}", clock);

    let final_word = ex11("hola".to_string(), "chao".to_string());
    println!("la palabra final queda: {}", final_word);

    ex10(2023);

     */

    println!("Ingrese los grados celsius:");
    let cel = ask_float();
    let fahr = ex12(cel);
    println!("{cel} equivalen a {fahr} fahrenheit");
    
}

// quitar vocales
fn ex01(text: String){
    // Definir un procedimiento que reciba una cadena de texto y devuelva
    // una cadena con las vocales eliminadas
    println!("El texto que se ha ingresado es {}", text);

    // definir un arreglo con las vocales, igual se puede hacer con if y else
    let vocales:[&str; 5] = ["a", "e", "i", "o", "u"];
	
    // Para saber en que letra se encuentra se debe sacar un Slice
    let mut i = 0;
    // Texto donde se iran guardando todos los caracteres NO vocal
    let mut final_text: String = "".to_string();
    loop {
	let character = &text[i..i+1];
	//println!("Se encuentra en la letra {}", character);
	
	//Se agrega
	if !(vocales.contains(&character)){
	   final_text.push_str(&character); 
	}
	
	if i == text.len()-1{
	    break;
	}
	
	i = i+1;
    }

    println!("El texto al final queda: {}", final_text);
}

// Tomar letra y texto y devolver cuantas veces se encuentra la letra en el texto
fn ex02(text: String, letra: String) -> i32{  
    let mut i: i32 = 0;
    for character in text.chars(){
	if character.to_string() == letra{
	    i += 1;
	}
    }
    i
}

// Contar el numero de vocales en la cadena
fn ex03(text: String){
    let mut count: i32 = 0;
    let vocales:[char; 5] = ['a', 'e', 'i', 'o', 'u'];
    for character in text.chars(){
	if vocales.contains(&character){
	    count += 1;
	}
    }
    println!("El texto contiene {count} vocales");
}

// ir diciendo mayor y menor a medida se ingresan numeros
fn ex04(){
    // Se van ingresando numeros, primero se pide el N del total de numeros a ingresar
    // a medida que se van ingresando se calcula el mayor y el menor, se muestran en pantalla

    let mut max: i32;
    let mut min: i32;
    let mut num: i32;
    
    // pedir el N total de numeros
    println!("Ingrese la cantidad de numeros a ingresar: ");
    let n: i32 = ask_number();

    // Pedir una vez desde afuera para inicializar el min y el max
    println!("Ingrese un numero: ");
    num = ask_number();
    min = num;
    max = num;
    report(min, max);
    
    // Seguir con el resto
    for _x in 1..n {
	println!("Ingrese un numero: ");
	num = ask_number();
	if num >= max{
	    max = num;
	} else if num <= min{
	    min = num;
	}
	report(min, max);
    }
}

// reportar los dos numeros
fn report(min: i32, max: i32){

    println!("Hasta el momento, el menor es {}", min);
    println!("Hasta el momento, el mayor es {}", max);
}

// Pedir un numero entero y devolverlo
fn ask_number() -> i32{

    let stdin = io::stdin();

    //println!("Ingrese un numero:");
    let mut user_input = String::new();
    stdin.read_line(&mut user_input).expect("expected to read line");
    let str_num: i32 = user_input.trim().parse().unwrap();
    str_num
}


// pedir numero entero
fn ask_float() -> f32{

    let stdin = io::stdin();

    //println!("Ingrese un numero:");
    let mut user_input = String::new();
    stdin.read_line(&mut user_input).expect("expected to read line");
    let str_num: f32 = user_input.trim().parse().unwrap();
    str_num
}

// conjetura ulam
fn unam_ex05(mut num: i32){

    println!("Numero inicial es {num}");
    while num != 1{
	if num % 2 == 0 {
	    num = num / 2;
	} else {
	    num = num*3 +1;
	}
	println!("Numero queda {num}");
    }
}

// tomar dos numeros y devolver el mayor
fn ex06(num1: i32, num2: i32) -> i32{
    if num1 > num2{
	num1
    }else if num1 < num2{
	num2
    }else{
	println!("Los numeros son iguales!");
	num1
    }
}

// devolver la primera letra de una cadena
fn ex07(text: String) -> String{

    // sacar el primer caracter
    let character = &text[0..1];
    character.to_string()
}


// devolver la ultima letra
fn ex08(text: String) -> String{
    let roof = text.len();
    let character = &text[roof-1..roof];
    character.to_string()
}


// Sumar horas y ver que hora sera
fn ex09(mut t_clock: i32, h_hours: i32) -> i32{
    // Se reciben horas entre 0 y 23
    // se saca el modulo h%24, se suma eso
    // por cada 24 horas no se agrega nada
    let sumando = h_hours%24;
    // si la suma supera 24 hay que pasar al siguiente dia
    t_clock = (t_clock + sumando)%24;
    t_clock
}

// Ver si el año es bisiesto o no
fn ex10(year: i32){
    if year >= 1582{
	let year_str = year.to_string();
	let roof = year_str.len();
	let last_two = &year_str[roof-2..roof];
	if last_two == "99"{
	    if year % 400 == 0 {
		// bisisesto
		println!("El año {} es bisiesto", year);
	    } else {
		// no es bisiesto
		println!("El año {} no es bisiesto", year);
	    }
	} else if year % 400 == 0{
	    // bisiesto
	    println!("El año {} es bisiesto", year);
	} else {
	    println!("El año {} no es bisiesto", year);
	}
    } else {
	if year % 4 == 0{
	    println!("El año {} es bisiesto", year);
	} else {
	    println!("El año {} no es bisiesto", year);
	}
    }
}


// ver cuales son las letras de la primera palabra que no estan en la segunda
fn ex11(word1: String, word2: String) -> String{
    let mut wfinal: String = "".to_string();
    for x in word1.chars(){
	if !(word2.contains(x)){ // no se agregan duplicados
	    wfinal = format!("{}{}", wfinal, x);
	}
    }
    wfinal
}

// de string a float
fn str_to_float(string: String) -> f32 {
    let trimmed = string.trim();
    let num: f32 = trimmed.parse().unwrap();
    num
}

// de celsius a fahrenheit
fn ex12(celsius: f32)-> f32{

    // f = c*1.8 + 32
    // (f-32)/1.8 = c
    let fa = celsius*1.8 + 32.0;
    fa
}
