// ejercicio 10

use std::io;
    
fn main(){
    let mut total: f32 = 0.0;

    let stdin = io::stdin();
    let mut count: f32 = 0.0;
    println!("Numero para seguir sumando, 'N' para parar:");
    loop{
	println!("Ingrese:");
	let mut user_input = String::new();
	stdin.read_line(&mut user_input).expect("expected to read line");

	if user_input == "N\n"{
	    break;
	} else {
	    let num: f32 = user_input.trim().parse().unwrap();
	    total = total + num;
	    count = count + 1.0;
	}
    }
    let prom = total/count;
    println!("El promedio es: {prom}");

}
