// ejercicio 8

use std::io;
    
fn main(){
    let mut total: i32 = 0;

    let stdin = io::stdin();
    println!("Numero para seguir sumando, 'N' para parar:");
    loop{
	println!("Ingrese:");
	let mut user_input = String::new();
	stdin.read_line(&mut user_input).expect("expected to read line");

	if user_input == "N\n"{
	    break;
	} else {
	    let num: i32 = user_input.trim().parse().unwrap();
	    total = total + num;
	}
    }
    println!("La suma total es: {total}");

}
