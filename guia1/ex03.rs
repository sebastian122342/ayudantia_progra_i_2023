// ejercicio 3

use std::io;

fn main(){

    let age: i32;
    let mut user_input = String::new();
    let stdin = io::stdin();

    println!("Ingresa tu edad:");
    
    stdin.read_line(&mut user_input).expect("expected to read line");
    age = user_input.trim().parse().unwrap();
    
    if age >= 18 {
	println!("Tienes suficiente edad para votar");
    } else {
	println!("No tienes suficiente edad para votar")
    }
}
