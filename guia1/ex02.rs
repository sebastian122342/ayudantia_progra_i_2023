// ejercicio 2

use std::io;

fn main(){


    let mut nm = String::new();
    let stdin = io::stdin();

    println!("Ingrese un nombre: ");
    stdin.read_line(&mut nm).expect("Expected to read line");
    println!("El nombre que se ha ingresado es {}", nm);
}
