// ejercicio 4

use std::io;

fn main(){

    println!("Ingresa un numero:");
    let num = ask_number();
    if num % 2 == 0 {
	println!("El numero es par");
    }else{
	println!("El numero es impar");
    }
    
}

// Pedir un numero entero y devolverlo
fn ask_number() -> i32{

    let stdin = io::stdin();

    //println!("Ingrese un numero:");
    let mut user_input = String::new();
    stdin.read_line(&mut user_input).expect("expected to read line");
    let num: i32 = user_input.trim().parse().unwrap();
    num
}

