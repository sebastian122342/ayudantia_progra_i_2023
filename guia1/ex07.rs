// ejercicio 7

use std::io;

fn main(){

    println!("Ingrese un numero: ");
    let num = ask_number();
    let fact_num = fact(num);
    println!("El factorial de {num} es {fact_num}");

}

// Pedir un numero entero y devolverlo
fn ask_number() -> i32{

    let stdin = io::stdin();

    //println!("Ingrese un numero:");
    let mut user_input = String::new();
    stdin.read_line(&mut user_input).expect("expected to read line");
    let str_num: i32 = user_input.trim().parse().unwrap();
    str_num
}

fn fact(num: i32) -> i32{

    let mut res: i32 = 1;
    for i in 1..num+1{
	res = res * i;
    }
    res
}
