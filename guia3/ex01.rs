// ejercicio 1
// funcion recursiva donde se calcule a a la potencia de b

fn power(a: i32, b: i32) -> i32{

    if b == 0{
	return 1
    }else{
	return a * power(a, b-1);
    }
}


fn main(){
    println!("hola");
    let a: i32 = 2;
    let b: i32 = 0;
    let num: i32 = power(a, b);
    println!("{a} elevado a {b} es {num}");
    
}
