// ejercicio 3

fn mcd(a: i32, b: i32) -> i32{

    if b == 0{
	return a;
    } else {
	let r = a % b;
	return mcd(b, r);
    }
}

fn main(){

    let a: i32 = 2366;
    let b: i32 = 273;
    let c: i32 = mcd(a, b);

    println!("{c}");
}
