// ejercicio 4

fn tribonacci(a: i32, b:i32, c:i32, limit:i32){

    if limit == 0{
	return
    } else {
	let new: i32 = a + b + c;
	println!("{new}");
	return tribonacci(new, a, b, limit-1);
    }
    
}

fn main(){
    
    tribonacci(1, 1, 2, 4);
}
