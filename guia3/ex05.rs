// ejercicio 5

fn rec_ulam(mut num: i32){

    println!("{num}");
    if num == 1{
	return;
    } else {
	if num % 2 == 0{
	    num = num / 2;
	} else {
	    num = num*3 + 1;
	}
	rec_ulam(num);
    }
}

fn main(){

    let num: i32 = 26;
    rec_ulam(num);
}
