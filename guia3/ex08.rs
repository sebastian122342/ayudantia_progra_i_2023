// ejercicio 8

use std::io;

fn main(){
    println!("Ingresar un numero");
    let stdin = io::stdin();
    let mut input = String::new();
    stdin.read_line(&mut input).unwrap();
    let num: i32 = match input.trim().parse(){
	Ok(numero) => numero,
	Err(_) => panic!("No se ingresó un numero"),
    };
    println!("{num}");
}
